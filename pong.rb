# PONG XP
# --------------
# A pong clone!
#
# Version 1.1
# Producer/Programmer/Story Writer/3D Artist/Music: RPG
# Composer: Willow

# Version History:
# Version 1.0	March 4, 2005
# - The first version~ 
# Version 1.1	March 6, 2005
# - Fixed a bug where the ball sometimes goes vertically through the paddle
# - Fixed a bug in ball collision with player where the ball bounced off too early
# - Fixed the bug where the player could move out of the screen bounds
# - Fixed some other small bugs
# - Added a background music composed by Willow (Thanks!)
# - Made the game a bit easier

# Story:
# The evil villain wanted to destroy the world and you are our only hope,
# you have challenged him to a game of pong.
# If you win, the world is saved, otherwise darkness awaits us all.
#
# Controls:
# Up/Down: Move paddles
# Left/Right: Adjust game speed
# Escape: Exit game
# Enter: Start the game
# Ctrl: Play/Stop music
#
# License
#
#   Copyright (c) 2005 Firas Assad
#
#   This software is provided 'as-is', without any express or implied
#   warranty. In no event will the authors be held liable for any damages
#   arising from the use of this software.
#
#   Permission is granted to anyone to use this software for any purpose,
#   including commercial applications, and to alter it and redistribute it
#   freely, subject to the following restrictions:
# 
#   1.  The origin of this software must not be misrepresented; you must 
#       not claim that you wrote the original software. If you use this 
#       software in a product, an acknowledgment in the product 
#       documentation would be appreciated but is not required.
#
#   2.  Altered source versions must be plainly marked as such, and 
#       must not be misrepresented as being the original software.
#
#   3.  This notice may not be removed or altered from any 
#       source distribution.
#

# Filled Sprite: This is just an easier to use version of sprite that
# has a single color filled bitmap
class FSprite < Sprite

  def initialize (color, dx, dy, width, height)
    # Initialize everything at once
    super(nil)
    @color = color
    self.x = dx
    self.y = dy
    self.bitmap = Bitmap.new (width, height)
    self.bitmap.fill_rect(self.bitmap.rect, @color)
  end

  def width
    # Return width of the bitmap (instead of sprite.bitmap.width)
    return self.bitmap.width
  end

  def height
    # Return height of the bitmap (instead of sprite.bitmap.height)
    return self.bitmap.height
  end

end

# All the magic is in this class!
class Game

  def initialize
    w = 5       # Width
    h = 75     # Height
    @xspeed = 8                               # Speed (Increase in x, also overall speed)
    @yspeed = @xspeed                 # Slope (increase in y)
    @victories = 0                              # Number of victories
    @defeats = 0                                # Number of defeats
    @waiting_input = true                 # Waiting for C to be pressed
    @player  = FSprite.new(Color.new(0,255,0), 0, 202, w, h)     # Player paddle
    @enemy = FSprite.new(Color.new(0,255,0), 635, 202, w, h) # Enemy paddle
    @line = FSprite.new(Color.new(0,255,0), 319, 0, 2, 480)       # Middle line
    @ball = FSprite.new(Color.new(255,255,255), 318, 238, 5, 5)  # The pong ball
    @text = FSprite.new(Color.new(0,0,0, 0), 270, 5, 100, 32)     # Score text
    @text.bitmap.font = Font.new("Arial", 22)                                  # Text font
    @text.bitmap.draw_text(0,0,100, 32, "PONG XP", 1)
  end

  def update
    # One frame update
    Graphics.update
    Input.update
    update_player
    update_enemy
    update_speed
    # wait for decision key (at start, after someone wins)
    if Input.trigger?(Input::C)
      @waiting_input = false 
      # Update score text
      @text.bitmap.clear
      @text.bitmap.draw_text(0,0,100, 32, 
                                                @victories.to_s + "    " + @defeats.to_s, 1)
    end
    return if @waiting_input
    update_ball
    update_collision
  end

  def update_speed
    # Change speed if right or left are pressed
    if Input.trigger?(Input::RIGHT)
      if @xspeed >= 0 
        @xspeed += 1
      else
        @xspeed -= 1
      end
      @yspeed = @yspeed >= 0 ? @xspeed.abs : -@xspeed.abs
    end
    if Input.trigger?(Input::LEFT)
      if @xspeed > 1
        @xspeed -= 1
      elsif @xspeed < -1
        @xspeed += 1
      end
      @yspeed = @yspeed >= 0 ? @xspeed.abs : -@xspeed.abs
    end
  end

  def update_player
    # Move player up and down depending on input
    if Input.press?(Input::UP)
      @player.y -= @xspeed.abs if @player.y > 0
    end
    if Input.press?(Input::DOWN)
      @player.y += @xspeed.abs if @player.y + @player.height < 480
    end
  end

  def update_enemy
    # After enemy hits the ball, it moves to the vertical center
    if @xspeed < 0
      @enemy.y += @xspeed.abs if @enemy.y < 240 - @enemy.height / 2
      @enemy.y -= @xspeed.abs if @enemy.y > 240 - @enemy.height / 2
      return
    end
    # There's a random chance that the enemy won't move in the right time.
    return if @ball.x < (200 + rand(270))
    # Move the enemy up and down depending on the relative position of ball
    if @ball.y + rand(@enemy.height / 2.5)< @enemy.y + @enemy.height / 2
      @enemy.y -= @xspeed.abs
    elsif @ball.y - rand(@enemy.height / 2.5) >=  @enemy.y + @enemy.height / 2
      @enemy.y += @xspeed.abs
    end
  end

  def update_collision
    # Check ball's collision
    check_collision_players
    check_collision_screen
  end

  def update_ball
    # Move ball by x and y speed
    @ball.y += @yspeed 
    @ball.x += @xspeed
  end

  def check_collision_screen
    # Check collision with top and bottom of screen
    if @ball.y >= 475 or @ball.y <= 0
      @yspeed = -@yspeed
    end
  end

  def check_collision_players
    # Check collision with enemy
    if @ball.x >= 630 and @ball.y > @enemy.y and @ball.y < @enemy.y + @enemy.height
      # Enemy collision, reverse ball direction
      @xspeed = -@xspeed
      # Prevent collision from happening again
      update_ball while @ball.x >= 630
    elsif @ball.x >= 635
      # Enemy couldn't hit the ball, player wins
      @victories += 1
      @ball.x = 318
      @ball.y = 238
      @xspeed = -@xspeed
      @text.bitmap.clear
      @text.bitmap.draw_text(0,0,100, 32, "You Win!", 1)
      # Wait for player input
      @waiting_input = true
    end
    # Check collision with player
    if @ball.x <= 6  and @ball.y >= @player.y and @ball.y <= @player.y + @player.height
      # Player collision, reverse ball direction
      @xspeed = -@xspeed
      # Prevent collision from happening again
      update_ball while @ball.x <= 5
    elsif @ball.x <= 5
      # Player couldn't hit the ball, enemy wins
      @defeats += 1
      @ball.x = 318
      @ball.y = 238
      @xspeed = -@xspeed
      @text.bitmap.clear
      @text.bitmap.draw_text(0,0,100, 32, "You Lose!", 1)
      # Wait for player input
      @waiting_input = true
    end
  end

end

begin
  Graphics.freeze
  Graphics.transition(20)
  # Create a new pong game
  pong =Game.new
  @bgm = true   # Is the BGM on or off?
  # Play some music~
  Audio.bgm_play('Data\PongTheme', 100, 100)
  while(true)
    # Update the game
    pong.update
    # CTRL turns music ON/OFF
    if Input.trigger?(Input::CTRL) and @bgm
      Audio.bgm_stop
      @bgm = false
    elsif Input.trigger?(Input::CTRL)
      Audio.bgm_play('Data\PongTheme', 100, 100)
      @bgm = true
    end
    # Cancel key exits the game
    break if Input.trigger?(Input::B)
  end
  Audio.bgm_fade(800)
rescue Errno::ENOENT
  # If an exception is caught, print the error message.
  print($!.message)
end
