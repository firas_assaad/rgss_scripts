A collection of Ruby Game Scripting System (RGSS) scripts for RPG Maker XP.
I wrote these around 2005-2008.
Check the comments at the top of each file for the license.